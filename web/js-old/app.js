function listUsers() {
    $.ajax({
        async: true,
        type: 'GET',
        contentType: 'application/json',
        url: '/json/user/list',
        dataType: 'json',
        success: function(data) {
            processUserList(data);
        },
        error: function(data) {
            console.log("list failed. response = " + data);
        }
    });
}