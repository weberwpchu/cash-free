function listBalances() {
    $.ajax({
        async: true,
        type: 'GET',
        contentType: 'application/json',
        url: '/json/balance/list',
        dataType: 'json',
        success: function(data) {
            processBalanceList(data);
            drawChart(data);
        },
        error: function(data) {
            console.log("list failed. response = " + data);
        }
    });
}

function drawChart(data) {
    var chart;

    var maxBalance = 0;
    for (var i=0; i<data.length; i++) {
        thisBalance = Math.abs(data[i].balance);
        if (thisBalance > maxBalance) {
            maxBalance = thisBalance;
        }
    }

    for (var i=0; i<data.length; i++) {
        var colorDec = 200 - Math.round(200 * Math.abs(data[i].balance) / maxBalance);
        var colorHex = colorDec.toString(16);
        if (colorHex.length == 1) {
            colorHex = '0' + colorHex;
        }
        if (data[i].balance > 0) {
            // green mix some red
            data[i].color = '#' + colorHex + 'FF00'
        } else {
            // red mix some green
            data[i].color = '#FF' + colorHex + '00'
        }
    }

    chart = new AmCharts.AmSerialChart();
    chart.dataProvider = data;
    chart.categoryField = "name";
    chart.rotate = true;
    chart.columnWidth = 0.9;
    chart.startDuration = 1;

    var categoryAxis = chart.categoryAxis;
    categoryAxis.gridAlpha = 0.1;
    categoryAxis.axisAlpha = 0;

    var roundedMax = Math.ceil(maxBalance);
    var axisMax = (roundedMax % 10) == 0 ? roundedMax : Math.ceil(roundedMax/10)*10;
    var valueAxis = new AmCharts.ValueAxis();
    valueAxis.maximum = axisMax;
    valueAxis.minimum = -1 * axisMax;
    valueAxis.gridAlpha = 0.1;
    valueAxis.axisAlpha = 0;
    valueAxis.unit = "$";
    valueAxis.unitPosition = "left";
    chart.addValueAxis(valueAxis);

    var graph = new AmCharts.AmGraph();
    graph.valueField = "balance";
    graph.type = "column";
    graph.lineAlpha = 0;
    graph.colorField = "color";
    graph.fillAlphas = 0.8;
    chart.addGraph(graph);

    $('#chartdiv').height((data.length*32)+'px');
    chart.write("chartdiv");
}

function processBalanceList(data) {
    var balanceListHtml = '';

    for (var i=0; i<data.length; i++) {
        data[i].balance = data[i].balance.toFixed(2);
        if (i==0) {
            balanceListHtml += '<tr class="error" style="font-size:20px">';
        } else {
            balanceListHtml += '<tr>';
        }
        var balanceStyle = '';
        if (data[i].balance < 0) {
            balanceStyle = ' style="color:red"';
        }
        balanceListHtml += '<td>' + data[i].name + '</td><td' + balanceStyle + '>$' + data[i].balance + '</td></tr>';
    }

    $('#balanceList').html(balanceListHtml);
}

$(function() {
    listBalances();
})