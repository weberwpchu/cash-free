function listPayments() {
    $.ajax({
        async: true,
        type: 'GET',
        contentType: 'application/json',
        url: '/json/payment/list',
        dataType: 'json',
        success: function(data) {
            processPaymentList(data);
        },
        error: function(data) {
            console.log("list failed. response = " + data);
        }
    });
}

function processPaymentList(data) {
    var paymentListHtml = '';

    for (var i=0; i<data.length; i++) {
        var total = 0.0;
        for (var j=0; j<data[i].paidFor.length; j++) {
            total += data[i].paidFor[j].amount;
        }

        for (var j=0; j<data[i].paidFor.length; j++) {
            if (j==0) {
                paymentListHtml += '<tr><td rowspan=' + data[i].paidFor.length + '>' + data[i].paidBy + '</td>';
                paymentListHtml += '<td rowspan=' + data[i].paidFor.length + '>$' + total.toFixed(2) + '</td>';
            } else {
                paymentListHtml += '<tr>'
            }

            var paidFor = data[i].paidFor[j];
            paymentListHtml += '<td>' + paidFor.name + '</td><td style="text-align: right">$' + paidFor.amount.toFixed(2) + '</td>';

            if (j==0) {
                paymentListHtml +=
                    '<td rowspan=' + data[i].paidFor.length + '>' + data[i].purpose + '</td>' +
                    '<td rowspan=' + data[i].paidFor.length + '>' + toHKTDate(data[i].timestamp) + '</td>' +
                    '</tr>';
            } else {
                paymentListHtml += '</tr>'
            }
        }
    }

    $('#paymentList').html(paymentListHtml);
}

function toHKTDate(utcString) {
    var splitDatetime = utcString.split(/-| |:/);
    var hktDate = new Date(splitDatetime[0], parseInt(splitDatetime[1])-1, splitDatetime[2], parseInt(splitDatetime[3])+8, splitDatetime[4], splitDatetime[5]);
    return hktDate.getFullYear() + '-' + zeroPad(hktDate.getMonth()+1) + '-' + zeroPad(hktDate.getDate()) + ' '
                    + zeroPad(hktDate.getHours()) + ':' + zeroPad(hktDate.getMinutes()) + ':' + zeroPad(hktDate.getSeconds());
}

function zeroPad(str) {
    if (str < 10) {
        return '0' + str;
    } else {
        return str;
    }
}

$(function () {
    listPayments();
});