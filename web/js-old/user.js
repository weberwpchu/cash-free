function addUser() {
    var userData = new Object();
    var nameInput = $("#name");
    userData.name = nameInput.val();
    nameInput.val("");



    $.ajax({
        async: false,
        type: 'POST',
        contentType: 'application/json',
        url: '/json/user/add',
        data: JSON.stringify(userData),
        dataType: 'json',
        success: function() {
        },
        error: function(data) {
            console.log("add failed. response = " + data);
        }
    });

    listUsers();
}

function processUserList(data) {
    var userListHtml = '';

    for (var i=0; i<data.length; i++) {
        userListHtml+= '<li>' + data[i].name + '</li>';
    }

    $('#user-list').html(userListHtml);
}

$(function() {
    listUsers();
})