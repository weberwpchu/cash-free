package com.cashfree.servlet;

import com.googlecode.objectify.ObjectifyService;
import com.cashfree.entity.PaidFor;
import com.cashfree.entity.Payment;
import com.cashfree.entity.User;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class StartupListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ObjectifyService.register(User.class);
        ObjectifyService.register(Payment.class);
        ObjectifyService.register(PaidFor.class);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
}
