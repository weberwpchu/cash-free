package com.cashfree.entity;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class Balance implements Comparable {
    @Id
    private Long id;
    private String name;
    private double balance;

    public Balance() {
        this.balance = 0;
    }

    public Balance(String name) {
        this();
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Balance{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                '}';
    }

    public void substract(double amount) {
        balance -= amount;
    }

    public void add(double amount) {
        balance += amount;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Balance) {
            Balance compareTo = (Balance) o;
            if (this.balance == compareTo.balance) {
                return this.name.compareTo(compareTo.name);
            } else {
                double diff = this.balance - compareTo.balance;
                if (diff > 0) {
                    return 1;
                } else if (diff < 0) {
                    return -1;
                } else {
                    return 0;
                }
            }
        } else {
            return 0;
        }
    }
}
