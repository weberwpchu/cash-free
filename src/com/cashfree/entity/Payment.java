package com.cashfree.entity;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.IgnoreSave;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Entity
public class Payment {
    @Id
    private Long id;
    private String paidBy;
    @IgnoreSave
    private Collection<PaidFor> paidFor = new ArrayList<PaidFor>();
    private transient List<Key<PaidFor>> paidForKeyList;
    @Index
    private Date timestamp;
    private String purpose;

    @OnSave
    private void onSave() {
        timestamp = new Date();
        paidForKeyList = new ArrayList<Key<PaidFor>>();
        for (PaidFor paidForSingle : paidFor) {
            Key<PaidFor> paidForKey = ofy().save().entity(paidForSingle).now();
            paidForKeyList.add(paidForKey);
        }
    }

    @OnLoad
    private void onLoad() {
        Map<Key<PaidFor>,PaidFor> paidForMap = ofy().load().keys(paidForKeyList);
        paidFor = paidForMap.values();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPaidBy() {
        return paidBy;
    }

    public void setPaidBy(String paidBy) {
        this.paidBy = paidBy;
    }

    public Collection<PaidFor> getPaidFor() {
        return paidFor;
    }

    public void setPaidFor(Collection<PaidFor> paidFor) {
        this.paidFor = paidFor;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", paidBy='" + paidBy + '\'' +
                ", paidFor=" + paidFor +
                ", timestamp=" + timestamp +
                ", purpose='" + purpose + '\'' +
                '}';
    }
}
