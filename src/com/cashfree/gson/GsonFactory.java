package com.cashfree.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonFactory {
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    private GsonFactory() {};

    public static Gson gson() {
        return gson;
    }
}
