'use strict';

angular.module('angularjsApp', [
  'ngRoute',
  'ngCookies',
  'ngResource',
  'ngSanitize'
])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/balance/list', {
        templateUrl: 'views/balance.html',
        controller: 'BalanceCtrl'
      })
      .when('/payment/list', {
        templateUrl: 'views/payment.html',
        controller: 'PaymentCtrl'
      })
      .when('/user/add', {
        templateUrl: 'views/user.html',
        controller: 'UserCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
