'use strict';

angular.module('angularjsApp')
  .controller('PaymentCtrl', function ($scope, $http) {
    var url = '/json/payment/list';
    $http.get(url).success(function (data) {
      var paymentPairs = [];

      data.forEach(function (payment) {
        var total = 0;
        payment.paidFor.forEach(function (borrower, index) {
          total += borrower.amount;
          var paymentPair = {
            "paidBy": payment.paidBy,
            "total": "0",
            "for": borrower.name,
            "amount": borrower.amount,
            "purpose": payment.purpose,
            "timestamp": payment.timestamp,
            "borrowerIndex": index,
            "totalBorrowers": payment.paidFor.length
          };
          paymentPairs.push(paymentPair);
        });

        paymentPairs[paymentPairs.length-payment.paidFor.length].total = total;
      });

      $scope.payments = paymentPairs;
    });
  });
