'use strict';

angular.module('angularjsApp')
  .controller('UserCtrl', function ($scope, $http) {
    $http.get('/json/user/list').success(function (data) {
      $scope.users = data;
      console.log(data);
    });

    $scope.duplicate = false;

    $scope.addUser = function () {
      console.log($scope.user);
      $http.post('/json/user/add', $scope.user);
    };

    $scope.checkUserDuplicate = function () {
      var duplicate = false;
      $scope.users.forEach(function (user) {
        if (user.name == $scope.user.name)
          duplicate = true;
      });
      $scope.duplicate = duplicate;
    };
  });
